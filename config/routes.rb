Rails.application.routes.draw do
  root 'application#angular'

  namespace :api do
    namespace :v1 do
      resources :products, except: %i(edit new)
    end
  end
end

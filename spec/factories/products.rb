FactoryGirl.define do
  factory :product do
    sequence(:name) {|n| "Product#{n}" }
    sequence(:price) {|p| p}
    description 'description'
  end
end

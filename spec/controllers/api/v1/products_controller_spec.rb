require 'rails_helper'

describe Api::V1::ProductsController do
  it 'sends a list of products' do
    create_list(:product, 12)
    get :index,  format: :json

    json = JSON.parse(response.body)
    expect(response).to be_success
    expect(json['products'].length).to eq(12)
  end

  it 'send a single product' do
    product = create(:product)

    get :show, id: product.id, format: :json

    json = JSON.parse(response.body)
    expect(response).to be_success
    expect(json['id']).to eq(product.id)
  end

  it 'create product' do
    post :create,
         products: {name: 'product_test', price: 12},
         format: :json

    json = JSON.parse(response.body)
    expect(response).to be_success
    expect(json['name']).to eq('product_test')
    expect(json['price']).to eq(12)
  end

  it 'delete product' do
    product = create(:product)

    delete :destroy, id: product.id, format: :json
    expect(response).to be_success
  end
end

require 'rails_helper'

describe Product do
  it 'should be valid factory' do
    expect(create(:product)).to be_valid
  end

  it 'invalid without name' do
    expect(build(:product, name: '')).to be_invalid
  end

  it 'invalid without price' do
    expect(build(:product, price: nil)).to be_invalid
  end
end

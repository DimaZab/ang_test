angular.module 'angTest'
  .filter 'startFrom', ->
    return (input, start) ->
      if input
        start = +start;
        return input.slice(start)

      return []

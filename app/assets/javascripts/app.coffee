angular.module 'angTest', ['templates', 'ui.router', 'appResource', 'ui.bootstrap']
  .run ($rootScope) ->
    $rootScope.$on '$stateChangeStart', (event, toState, toParams) ->
      $rootScope.state =  toState.name
      $rootScope.params = toParams

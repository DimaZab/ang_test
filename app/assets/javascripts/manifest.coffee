#= require angular
#= require angular-ui-router/release/angular-ui-router.min
#= require angular-rails-templates.js.erb
#= require angular-bootstrap/ui-bootstrap.min
#= require angular-bootstrap/ui-bootstrap-tpls.min

#= require angularjs/rails/resource

#= require_tree ./modules
#= require app
#= require routes

#= require_tree ./models
#= require_tree ./templates
#= require_tree ./directives
#= require_tree ./controllers
#= require_tree ./filters

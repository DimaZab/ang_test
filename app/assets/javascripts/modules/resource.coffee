angular.module("appResource", [ "rails" ]).factory "AppModel", [
  "RailsResource",
  (RailsResource) ->

    class AppModel extends RailsResource

    AppModel.resourceUrl = (previous)->
      "/api/v1" + RailsResource.resourceUrl.apply @, arguments

    AppModel
]

angular.module 'angTest'
  .directive 'productForm', ->
    scope:
      submitFn: '='
      product: '='
      submitBtnText: '@'
    templateUrl: 'products/_form.html'

angular.module('angTest')
  .config([
    '$stateProvider',
    '$urlRouterProvider',
    '$locationProvider',
    ($stateProvider, $urlRouterProvider, $locationProvider) ->

      $stateProvider
        .state 'products',
          abstract: true,
          templateUrl: 'layouts/products.html'
        .state 'products.index',
          url: '/'
          templateUrl: 'products/index.html'
          controller: 'IndexProductsCtrl'
        .state 'products.show',
          url: '/{id:int}'
          templateUrl: 'products/show.html'
          controller: 'ShowProductsCtrl'
        .state 'products.new',
          url: '/new'
          templateUrl: 'products/new.html'
          controller: 'NewProductsCtrl'
        .state 'products.edit',
          url: '/{id}/edit'
          templateUrl: 'products/edit.html'
          controller: 'EditProductsCtrl'


      $urlRouterProvider.otherwise '/'

      # $locationProvider.html5Mode
      #   enabled: true
      #   requireBase: false
      #   html5Mode: true
  ])

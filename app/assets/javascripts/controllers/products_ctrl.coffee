angular.module 'angTest'
  .controller 'IndexProductsCtrl', [
    '$scope', 'Product',
    ($scope, Product) ->
      Product.query().then (products) ->
        $scope.products = products
        $scope.totalItems = $scope.products.length

      $scope.currentPage = 1
      $scope.itemsPerPage = 5
      $scope.itemsPerPa = $scope.itemsPerPage
      $scope.noOfPages = Math.ceil($scope.totalItems / $scope.itemsPerPage)
  ]

  .controller 'ShowProductsCtrl', [
    '$scope', 'Product', '$stateParams',
    ($scope, Product, $stateParams) ->
      Product.get($stateParams.id).then (product) -> $scope.product = product
  ]

  .controller 'NewProductsCtrl', [
    '$scope', 'Product', '$state',
    ($scope, Product, $state) ->
      $scope.product = {}

      $scope.submit = ->
        console.log 'asd'
        new Product($scope.product).create().then(
          (product) -> $state.go('products.show', {id: product.id})
        )
  ]

  .controller 'EditProductsCtrl', [
    '$scope', 'Product', '$state', '$stateParams',
    ($scope, Product, $state, $stateParams) ->
      Product.get($stateParams.id).then (product) -> $scope.product = product

      $scope.submit = ->
        $scope.product.update().then(
          (product) -> $state.go('products.show', {id: product.id})
        )

      $scope.delete = ->
        console.log 'asd'
        $scope.product.delete().then(
          () -> $state.go('products.index')
        )
  ]

class Api::V1::ProductsController < Api::V1::BaseController
  respond_to :json
  actions :all, except: %i(edit new)

  private

  def product_params
    params
      .require(:products)
      .permit(:name, :price, :description)
  end
end
